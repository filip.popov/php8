<?php

function devide($a, $b)
{
//    if ($b === 0) {
//        throw new Exception("Can't divide on 0");
//    }
//
//    return $a / $b;

    return $b !== 0 ?  $a / $b : throw new Exception("Can't divide on 0");
}

try{
    devide(1, 0);
} catch (Exception $e) {
    echo '<pre>'; var_dump($e->getMessage());
}

try{
    devide(1, 0);
} catch (Exception) {
    echo '<pre>'; var_dump('Problem, please check!');
}
