<?php


class PersonOldWay
{
    public string $firstname;
    public string $lastname;
    public int $age;

    public function __construct(string $firstname, string $lastname, int $age)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->age = $age;
    }
}

class PersonNewWay
{
    public function __construct(
        public string $firstname,
        public string $lastname,
        public int $age
    ) {}
}

echo '<pre>'; var_dump(new PersonOldWay('Philip', 'Popov', 36));
echo '<pre>'; var_dump(new PersonNewWay('Philip', 'Popov', 36));
