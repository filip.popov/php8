<?php

//function set_value(mixed $a)
//{
//
//}
//
//function get_value() : mixed
//{
//    return null;
//}
//
//set_value(1);
//get_value();

class Person
{
    public mixed $someData;

    public function __construct(mixed $someData)
    {
        $this->someData = $someData;
    }

    public function getSomeData(): mixed
    {
        return $this->someData;
    }
}

$personInt = new Person(1);
echo '<pre>'; var_dump($personInt->getSomeData());
$personString = new Person('test');
echo '<pre>'; var_dump($personString->getSomeData());
$personNull = new Person(null);
echo '<pre>'; var_dump($personNull->getSomeData());
