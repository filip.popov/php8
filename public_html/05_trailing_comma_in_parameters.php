<?php

// if we use php 7.4 or lower, we cannot have comma after last parameter, but with php 8 we can
function hello($name, $age,)
{
    echo "Hello $name -> $age";
}

hello('Zura', 28);
