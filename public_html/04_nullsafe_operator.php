<?php

//// php7.4
//$country =  null;
//
//if ($session !== null) {
//    $user = $session->user;
//
//    if ($user !== null) {
//        $address = $user->getAddress();
//
//        if ($address !== null) {
//            $country = $address->country;
//        }
//    }
//}




// php8.0
//$country = $session?->user?->getAddress()?->country;

class Person
{
    private ?Country $country;

    public function __construct(?Country $country)
    {
        $this->country = $country;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }
}

class Country
{
    private string $name;

    private array $cities;

    public function __construct(string $name, array $cities)
    {
        $this->name = $name;
        $this->cities = $cities;
    }

    public function getCities(): array
    {
        return $this->cities;
    }

    public function getName(): string
    {
        return $this->name;
    }
}

$bulgaria = new Country('Bulgaria', ['Ruse', 'Sofia']);

$personWithCountry = new Person($bulgaria);

$personWithoutCountry = new Person(null);

echo '<pre>'; var_dump($personWithCountry?->getCountry()?->getName());

echo '<pre>'; var_dump($personWithoutCountry?->getCountry()?->getName());

