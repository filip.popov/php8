<?php

class Product
{
    const EVENT_CREATED = 'created';
    const EVENT_DELETED = 'deleted';
}


class ProductCreated
{
}

class ProductDeleted
{
}

#[\Attribute(\Attribute::TARGET_METHOD)]
class ListenTo
{
    public string $event;

    public function __construct(string $event)
    {
        $this->event = $event;
    }
}

#[\Attribute]
class SampleAttribute
{

}


#[Attribute]
class ProductSubscriber
{
    #[
        ListenTo(Product::EVENT_CREATED),
        SampleAttribute,
    ]
    public function onProductCreated(ProductCreated $event)
    { /* … */
    }

    #[ListenTo(Product::EVENT_DELETED)]
    public function onProductDeleted(ProductDeleted $event)
    { /* … */
    }
}

$reflectionClass = new ReflectionClass(ProductSubscriber::class);
$m = $reflectionClass->getMethod('onProductCreated');
echo '<pre>'; var_dump($m->getAttributes()[0]->getName());
echo '<pre>'; var_dump($m->getAttributes()[0]->getArguments());

echo '<pre>'; var_dump($m->getAttributes()[1]->getName());
echo '<pre>'; var_dump($m->getAttributes()[1]->getArguments());

$m2 = $reflectionClass->getMethod('onProductDeleted');
echo '<pre>'; var_dump($m2->getAttributes()[0]->getName());
echo '<pre>'; var_dump($m2->getAttributes()[0]->getArguments());
