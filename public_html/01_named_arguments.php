<?php

$numbers = [1, 2, 3, 4, 5, 6];

$evens = array_filter($numbers, function ($n){
    return $n % 2 === 0;
});

$squares = array_map(function ($n){
    return $n * $n;
}, $numbers);

echo '<pre>'; var_dump($evens, $squares);

$evens = array_filter(array: $numbers, callback: function($n){
    return $n % 2 === 0;
});

$squares = array_map(array:  $numbers, callback: function($n) {
    return $n * $n;
});

echo '<pre>'; var_dump($evens, $squares);

$pos = strpos( needle: "World", haystack: "Hello World",);
echo '<pre>'; var_dump($pos);

function customFunction(string $name, int $age) : string {
    return "$name -> $age";
}

$result = customFunction(age: 10, name: 'Ivan');

echo '<pre>'; var_dump($result);
