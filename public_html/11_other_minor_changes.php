<?php

// ::class can be called on objects as well
$foo = new DateTime();

var_dump(DateTime::class);
var_dump($foo::class);


// new string methods: str_contains, str_starts_with, str_ends_with
if (strpos('string with lots of words', 'words') !== false) {
    echo '<pre>'; var_dump('from old way with "strpos"');
}

if (str_contains('string with lots of words', 'words')) {
    echo '<pre>'; var_dump('from new way with "str_contains"');
}

echo '<pre>'; var_dump(str_starts_with('haystack', 'hay')); // true

echo '<pre>'; var_dump(str_ends_with('haystack', 'stack')); // true















