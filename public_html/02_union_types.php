<?php

class SampleClass {
    /**
     * @var int|float
     */
    public int|float $number;

    public function setNumber(int|float $number)
    {
        $this->number = $number;
    }

    public function getNumber(): int|float
    {
        return $this->number;
    }
}

$simpleClassSetInt = new SampleClass();
$simpleClassSetInt->setNumber(1);

echo '<pre>'; var_dump($simpleClassSetInt->getNumber());

$simpleClassSetFloat = new SampleClass();
$simpleClassSetFloat->setNumber(1.5);

echo '<pre>'; var_dump($simpleClassSetFloat->getNumber());
