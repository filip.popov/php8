<?php

// Example 1
class Product
{
    public function __destruct()
    {
        echo "Destroyed<br>";
    }

    public function id()
    {
        return rand(1, 10);
    }
}

//$p1 = new Product();
//$reviews = new WeakMap();
//$reviews[$p1] = [5, 5, 4];
//
//echo '<pre>';
//var_dump($reviews);
//echo '</pre>';
//
//unset($p1);
//echo "Unset<br>";
//
//echo '<pre>';
//var_dump($reviews);
//echo '</pre>';


// Example 2
class ReviewList
{
    private WeakMap $cache;

    public function __construct()
    {
        $this->cache = new WeakMap();
        echo '<pre>'; var_dump($this->cache);
    }

    public function getReviews(Product $prod): string
    {
        return $this->cache[$prod] ??= $this->findReviews($prod->id());
    }

    protected function findReviews(int $prodId): string
    {
        return '';
    }
}


$reviewList = new ReviewList();
$prod1 = new Product();
$prod2 = new Product();

$reviews_p1 = $reviewList->getReviews($prod1);
$reviews_p2 = $reviewList->getReviews($prod2);

// ...

//$reviews_p1_again = $reviewList->getReviews($prod1);

unset($prod1);

